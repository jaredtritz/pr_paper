

def make_table(header, subheader, entries):
    ts = """
    <table style="border: 1px solid green;" >
        <tr>
            <td align='center' style="padding:0px 5px 5px 5px; border-bottom:1pt solid black;">
                <strong>"""+header+"""</strong>
                <br>
                <font size="2">"""+subheader+"""</font>
            </td>
        </tr>
        <tr>
            <td>
                <table width='100%' style="border-collapse:collapse;">
        """
    for ee in entries:
        ts += """<tr><td>&bull; """+str(ee[0])+"""</td><td style="padding:0px 0px 0px 15px;">"""+str(ee[1])+"""</td></tr>"""
    ts += """
                </table>
            </td>
        </tr>
    </table>
    <br>"""
    return ts

print make_table(
    'header', 
    'Some more words', 
    [
        ['type', 'name'],
        ['type', 'name'],
        ['type', 'name']
    ]
    )

print make_table(
    'Courses', 
    'Course identity', 
    [
        ['id', 'int'],
        ['name', 'string'],
    ]
    )

print make_table(
    'Topics', 
    'Topic identity', 
    [
        ['id', 'int'],
        ['name', 'string'],
    ]
    )

print make_table(
    'Problems', 
    'Problem identity', 
    [
        ['id', 'int'],
        ['name', 'string'],
        ['url', 'string'],
        ['answer', 'int'],
    ]
    )

print make_table(
    'Users', 
    'User and role identity', 
    [
        ['id', 'int'],
        ['name', 'string'],
        ['staff', 'boolean'],
    ]
    )

print make_table(
    'Responses', 
    'Transaction Registry', 
    [
        ['id', 'int'],
        ['user_id', 'int'],
        ['problem_id', 'int'],
        ['answer', 'int'],
        ['start-time', 'datetime'],
        ['end-time', 'datetime'],
    ]
    )

print make_table(
    'Answer Choices', 
    'Choices per problem', 
    [
        ['id', 'int'],
        ['problem_id', 'int'],
        ['choice', 'int'],
        ['count', 'int'],
    ]
    )


